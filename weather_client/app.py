import requests
import os

import streamlit as st
import pandas as pd

from dotenv import load_dotenv

load_dotenv()

url = os.getenv("API_URL")

st.title("Weather ⛅")


def load_data():
    try:
        response = requests.get(url)

        data = response.json()

        df = pd.DataFrame(data)
        df.set_index("id")

        return df
    except requests.exceptions.HTTPError as errh:
        st.error("Erro ao realizar a requisição", icon="🚨")
    except requests.exceptions.ConnectionError as errc:
        st.error("Não foi possível conectar ao servidor", icon="🚨")
    except requests.exceptions.Timeout as errt:
        st.error("O tempo de resposta do servidor ultrapassou o limite", icon="🚨")
    except requests.exceptions.RequestException as err:
        st.error("Ocorreu um erro, por favor tente novamente", icon="🚨")
        st.exception(err)


with st.spinner("Carregando dados..."):
    df = load_data()

st.header("Dados do tempo")

st.dataframe(
    df,
    hide_index=True,
    column_order=("id", "meantemp", "humidity", "wind_speed", "meanpressure"),
)

st.header("Inserir novos dados")

with st.form("new_weather_info", clear_on_submit=True):
    meantemp = st.text_input("Média de temperatura")

    humidity = st.text_input("Humidade")

    wind_speed = st.text_input("Velocidade do vento")

    meanpressure = st.text_input("Média de pressão")

    submitted = st.form_submit_button("Enviar")

    if submitted:
        response = requests.post(
            url,
            json={
                "meantemp": meantemp,
                "humidity": humidity,
                "wind_speed": wind_speed,
                "meanpressure": meanpressure,
            },
        )

        if response.status_code == 201:
            st.success("Informação de clima enviada com sucesso!")
        elif response.status_code == 400:
            st.warning("preencha todos os campos")
        else:
            st.error("Ocorreu um erro, tente novamente mais tarde")
